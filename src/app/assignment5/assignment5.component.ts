import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../services/map.service1';
@Component({
  selector: 'app-assignment5',
  templateUrl: './assignment5.component.html',
  styleUrls: ['./assignment5.component.css']
})
export class Assignment5Component implements OnInit {
  @ViewChild('divMap') divMap!: ElementRef
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    this.mapService.initialMap(this.divMap)
  }
}
