import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../services/map.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  @ViewChild('divMap') divMap!: ElementRef
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.mapService.initialMap(this.divMap)
  }

}
