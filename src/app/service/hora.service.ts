import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HoraService {
  sum: number = 0

  constructor() { }

  calculate(nameList: string[] = []) {
    this.sum = 0
    for (let i = 0; i < nameList.length; i++) {
      this.sum = this.sum + nameList[i].charCodeAt(0)
    }
    this.sum = this.sum % 5
  }
}