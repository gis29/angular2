import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatsComponent } from './cats/cats.component';
import { FormsModule } from '@angular/forms';
import { Assignment1Component } from './assignment1/assignment1.component';
import { CommentComponent } from './assignment1/comment/comment.component';
import { Assignment3Component } from './assignment3/assignment3.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule} from 'primeng/button'
import {InputTextModule} from 'primeng/inputtext';
import { Assignment2Component } from './assignment2/assignment2.component';
import { LocateComponent } from './assignment2/locator/locate/locate.component';
import { TestComponent } from './test/test.component';
import { TableComponent } from './assignment4/table/table.component';
import { Assignment4Component } from './assignment4/assignment4.component';
import { Assignment5Component } from './assignment5/assignment5.component';
@NgModule({
  declarations: [
    AppComponent,
    CatsComponent,
    Assignment1Component,
    CommentComponent,
    Assignment3Component,
    Assignment2Component,
    LocateComponent,
    TestComponent,
    TableComponent,
    Assignment4Component,
    Assignment5Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ButtonModule,
    InputTextModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
