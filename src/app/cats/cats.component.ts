import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.css']
})
export class CatsComponent implements OnInit {

  @Input() color:string = "Black"
  @Output() meaw = new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }

  onClickMeaw(): void {
    this.meaw.emit('Hello')
  }

}
