import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Assignment1Component } from './assignment1/assignment1.component';
import { Assignment2Component } from './assignment2/assignment2.component';
import { Assignment3Component } from './assignment3/assignment3.component';
import { Assignment4Component } from './assignment4/assignment4.component';
import { Assignment5Component } from './assignment5/assignment5.component';
import { CatsComponent } from './cats/cats.component';
import { TestComponent } from './test/test.component';


const routes: Routes = [
  { path: 'assignment1', component: Assignment1Component },
  { path: 'cats', component: CatsComponent },
  { path: 'assignment3', component: Assignment3Component },
  { path: 'assignment2', component: Assignment2Component },
  { path: 'test', component: TestComponent },
  { path: 'assignment4', component: Assignment4Component },
  { path: 'assignment5', component: Assignment5Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
