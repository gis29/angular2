import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HoraService } from '../service/hora.service';

@Component({
  selector: 'app-assignment1',
  templateUrl: './assignment1.component.html',
  styleUrls: ['./assignment1.component.css']
})
export class Assignment1Component implements OnInit {
  name:string = " "
  comment:string= " "
  commentList:comment[] = []
  constructor( public horaService: HoraService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void{

  }

  doComment(){
    this.commentList.push({
      name : this.name,
      comment : this.comment
    })
  }

  doClear(){
    this.comment = " "
    this.name = " "
  }
}

interface comment{
  name: string,
  comment: string
}
