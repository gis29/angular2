import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from 'src/app/services/map.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @ViewChild('divMap') divMap!: ElementRef
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    this.mapService.initialMap(this.divMap)
  }
}
