import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from '../services/map.service';

@Component({
  selector: 'app-assignment4',
  templateUrl: './assignment4.component.html',
  styleUrls: ['./assignment4.component.css']
})
export class Assignment4Component implements OnInit {
  states: {region:string, state_name:string, abbr:string}[] = []
  constructor(private mapService: MapService) { }

  ngOnInit(): void {
  }

  onlocatorClick(data: any){
    console.log(data)
  }

  ngAfterViewInit(){
    this.mapService.queryAllStateObs().subscribe((response) => {
      console.log("queryAllStateObs response = ", response)
      this.states = response
    })
  }

  doStateClick(stateName: string){
    this.mapService.queryStateByStateName(stateName).then((response)=>{
      console.log("queryStateBystateName response=", response)
      if(response.features.length>0){
        let feature = response.features[0]
        this.mapService.addPolygonToGraphicLayer(feature.geometry)
      }
    })
  }

}
