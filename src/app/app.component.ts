import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MapService } from './services/map.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'My First Angular App';
  name = 'MmodpowW'
  isShow: boolean = true
  colors : string[] = ["Black","Brown","White"]

  @ViewChild('divMap') divMap!: ElementRef
  constructor(private mapService: MapService){}
  ngOnInit(): void{

  }

  callFn(message: string):void{
    alert(message)
  }

  setName(name: string){
    this.name = name
  }

  // ngAfterViewInit(){
  //   this.mapService.initalMap(this.divMap)
  // }
}

